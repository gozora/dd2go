All contributions to this projects are welcome although I can't guarantee they 
will be merged.

Should you decide to contribute bug-fix or feature that might might take 
significant amount of time, try to consult your intention prior you start
writing it. Good communication can save a lot of time.

Either way, thanks in advance for you contributions.
