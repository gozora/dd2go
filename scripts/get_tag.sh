#!/usr/bin/env bash

cd /code/go/dd2go || exit

git_bin=$(type -p git)

if test -z "$git_bin"; then
	echo "$0: Can't find git binary."
	exit 1
fi

tag=$(git name-rev --name-only --tags HEAD)
if [ "$tag" = "undefined" ]; then
	tag=$(git log --pretty=format:"%h" -1)
fi

echo "$tag"
