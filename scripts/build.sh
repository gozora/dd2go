#!/usr/bin/env bash

workdir="/go/src/dd2go"
cd "$workdir" || exit

tag=$(./scripts/get_tag.sh)

# shellcheck disable=SC2181
[ $? -gt 0 ] && echo "Failed to obtain tag." && exit 1

export CGO_ENABLED=0
export GOFLAGS=-ldflags="-X=gitlab.com/gozora/dd2go/cmd.version=$tag"

if test -z "$1"; then
	echo "Building ..."
	go build
else
	echo "Installing ..."
	go install
fi
