#!/usr/bin/env bash

# addr="172.17.0.2"
addr="127.0.0.1:8080"
curl_opts="--silent"

while read -r line; do
	curl_sum=$(curl $curl_opts http://"${addr}/${line}" | sha1sum \
		| awk '{print $1}')
	local_sum=$(sha1sum "$line" | awk '{print $1}')

	if [ "$curl_sum" = "da3968197e7bf67aa45a77515b52ba2710c5fc34" ]; then
		echo "$line - 404"
		continue
	fi

	echo -n "$line - "
	if [ "$curl_sum" = "$local_sum" ]; then
		echo "OK"
	else
		echo "FAILED"
	fi

done < "$1"
