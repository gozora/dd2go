# dd2go
## Table of content
`[TOC]`

## Description
dd2go is utility to create and serve deduplicated read-only group of files 
(archive) over HTTP.

## Limitations
**Linux**
- hard links will be not preserved when running _extract_ workflow
- file/directory permissions are not saved

**Windows**
- wildcards are not working in _create_ workflow (see [Roadmap](#roadmap))
- symbolic links from archives created on Linux are extracted as regular files

## Installation
- download and run latest pre-build [binaries](https://gitlab.com/gozora/dd2go/-/releases/permalink/latest)

OR

- build and install from source
```bash
$ git clone https://gitlab.com/gozora/dd2go.git
$ cd dd2go
$ go install
```

## Usage
`$ dd2go --help`
```
Create and serve de-duplicated archive over HTTP.

Usage:
  dd2go [command]

Examples:
Create archive:
   $ dd2go create --verbose  /bin/b* /tmp/p*
Serve created archive using HTTP:
   $ dd2go serve
List archive content:
   $ dd2go list
Extract content from archive:
   $ dd2go extract -v /bin/b*

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  create      Create new de-duplicated archive
  dump        Dump master index in JSON format
  extract     Extract file from archive
  help        Help about any command
  list        List content of archive
  serve       Serve provided de-duplicated archive using HTTP

Flags:
  -a, --archive string   Name of archive to create (default "archive.db")
  -b, --block-size int   Block size for file reading. (default 4096)
  -D, --debug            Turn on debug messages
  -h, --help             help for dd2go
  -s, --silent           Do not output anything
  -v, --version          version for dd2go

Use "dd2go [command] --help" for more information about a command.
```

## Getting started

### Patterns
_create_ and _extract_ commands support [patterns](https://pkg.go.dev/path/filepath#Match), 
hence when creating or extracting files from archive one can
substitute multiple characters with wildcards. (Using patterns during _create_ 
workflow unfortunately doesn't seems to work on Windows. Fix is planned.
See: [Roadmap](#roadmap))

Following [patterns](https://pkg.go.dev/path/filepath#Match) are allowed:
```
pattern:
   { term }
term:
   '*'         matches any sequence of non-Separator characters
   '?'         matches any single non-Separator character
   '[' [ '^' ] { character-range } ']'
               character class (must be non-empty)
   c           matches character c (c != '*', '?', '\\', '[')
   '\\' c      matches character c

character-range:
   c           matches character c (c != '\\', '-', ']')
   '\\' c      matches character c
   lo '-' hi   matches character c for lo <= c <= hi
```

### Create new archive
One can create archive using _create_ command. During creation, full or relative
path to file or directory can be specified. When relative path to file or 
directory is used, _dd2go_ will use current working directory (CWD) to prefix the
relative path.  
In example below, CWD is set to be the root (/) directory.

`$ dd2go create --archive /tmp/archive.db --verbose /bin/c* tmp/script.*`

```
Creating archive /tmp/archive.db with block size 4096 bytes.
Add [f]: /bin/cat
Add [f]: /bin/cat_copy
Add [f]: /bin/chgrp
Add [f]: /bin/chmod
Add [f]: /bin/chown
Add [f]: /bin/cp
Add [f]: /go/src/dd2go/tmp/script.ps1
Add [f]: /go/src/dd2go/tmp/script.sh
Finishing storage of master index.
Deduplicated 11 of total 111 blocks in 8 files.
```

When relative path to file or directory is used, and one would like use 
different prefix directory than current working directory (CWD) for file in 
archive, `--prefix-dir=directory` can be used. Directory used as value for 
`--prefix-dir` must begin with slash (/). See example below.

`$ dd2go create --archive /tmp/archive.db --verbose --prefix-dir=/hello-world /bin/c* tmp/script*`
```
Creating archive /tmp/archive.db with block size 4096 bytes.
Add [f]: /bin/cat
Add [f]: /bin/cat_copy
Add [f]: /bin/chgrp
Add [f]: /bin/chmod
Add [f]: /bin/chown
Add [f]: /bin/cp
Add [f]: /hello-world/tmp/script.ps1
Add [f]: /hello-world/tmp/script.sh
Add [l]: /hello-world/tmp/script_link.sh
Finishing storage of master index.
Deduplicated 11 of total 111 blocks in 9 files.
```

### List archive content
To list contents of archive, _list_ command can be used. Listing archive is 
specially useful for crafting of HTTP request, since there is currently no 
mechanism nor plan to implement command to remotely ask for file index.

`$ dd2go list --archive /tmp/archive.db`
```
[F] /bin/cat
[F] /bin/cat_copy
[F] /bin/chgrp
[F] /bin/chmod
[F] /bin/chown
[F] /bin/cp
[F] /hello-world/tmp/script.ps1
[F] /hello-world/tmp/script.sh
[L] /hello-world/tmp/script_link.sh -> script.sh
```

### Extract files from archive
Files can be extracted from archive using _extract_ command, but there might be
some [limitations](#limitations).
When _--dst-dir__ option is not specified, current working directory (CWD) will
be used.

`$ dd2go extract --dst-dir /tmp/extracted --verbose --archive /tmp/archive.db "/"`
```
Loading data...
Extracting file: /bin/cat
Extracting file: /bin/cat_copy
Extracting file: /bin/chgrp
Extracting file: /bin/chmod
Extracting file: /bin/cp
Extracting file: /bin/chown
Extracting file: /hello-world/tmp/script.ps1
Extracting file: /hello-world/tmp/script.sh
Extracting file: /hello-world/tmp/script_link.sh
Extracted 8 files into /tmp/extracted directory.
```

### Serve archive content over HTTP
`$ dd2go serve --archive /tmp/archive.db --listen :8080`
```
Loading data...
Starting web server on interface :8080
```

### Requesting data from remote HTTP server
#### Save file
`$ curl http://dd2go-remote:8080/bin/cat > /tmp/cat`
```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 43936  100 43936    0     0  78836      0 --:--:-- --:--:-- --:--:-- 78738

```

#### Execute script
- Linux  
`$ curl http://dd2go-remote:8080/hello-world/tmp/script.sh | bash`
```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    42  100    42    0     0  81395      0 --:--:-- --:--:-- --:--:-- 42000
Hello, World!
```

- Windows (PowerShell)  
`PS C:\> iex ((New-Object System.Net.WebClient).DownloadString('http://dd2go-remote:8080/hello-world/tmp/script.ps1'))`
```
Hello, World!
```

## Support
Please open [new Issue](https://gitlab.com/gozora/dd2go/-/issues/new) 
in case of:
 - questions
 - bug reports
 - feature requests

I'll do my best to answer as soon as I have time.

## Contributing
[CONTRIBUTING.md](CONTRIBUTING.md)

## Roadmap
- [X] Build packages with GitLab CI/CD
- [ ] HTTPS (?)
- [ ] Wildcards on Windows for _create_ workflow
- [X] Make links to directories work

## License
dd2go is licensed under [MIT License](LICENSE).

## Packages
Worth mentioning packages used by dd2go includes mostly [Golang standard library 
packages](https://pkg.go.dev/std) and [Cobra](https://pkg.go.dev/github.com/spf13/cobra).

### dedupfs
[dedupfs](https://pkg.go.dev/gitlab.com/gozora/dedupfs) package was 
created to provide functionality for dd2go, but was published and will be 
developed separately, as I did not find any project that would provide Golang 
fs.FS interface implementation with deduplication feature.

Although developed separately, stability, performance and security of dd2go 
will always be a priority.

## Project status
Project is active and some features will be added in the future.
For more details see [Roadmap](#roadmap).
