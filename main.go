package main

import (
	"gitlab.com/gozora/dd2go/cmd"
)

func main() {
	cmd.Execute()
}
