package cmd

import (
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/gozora/dd2go/myhttp"
	"gitlab.com/gozora/dedupfs"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

// TODO: Implement SSL (HTTPS) ?

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Aliases: []string{"server"},
	Use:     "serve",
	Short:   "Serve provided de-duplicated archive using HTTP",
	Long: `Make provided de-duplicated archive available over HTTP on port 80.
Port is currently not configurable and HTTPS is not available (for now). 
These options will be added later.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		printIf.NotSilent.Println("Loading data...")
		myFs, err := dedupfs.Open(archiveName)
		if err != nil {
			return err
		}
		defer myFs.Destroy()

		l, err := cmd.Flags().GetString("listen")
		if err != nil {
			return err
		}

		printIf.NotSilent.Printf("Starting web server on interface %v\n", l)

		// origHandler is standard handler that would be used during normal
		// file serving operations.
		origHandler := http.FileServer(http.FS(myFs))

		// wrapHandler was primarily created to enable logging of client request
		// IP addresses. It can be also used to intercept and modify client
		// request before they reach filesystem (DedupFS).
		// origHandler is needed here as parameter because its methods are called
		// once myhttp.wrapHandler.ServeHTTP() finished. (It is wrapped.)
		wrapHandler := myhttp.WrapHandler(origHandler)

		return http.ListenAndServe(l, wrapHandler)
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)

	serveCmd.Flags().StringP("listen", "l", ":80", "Listen for incoming connections on specific IP address and port.")
}
