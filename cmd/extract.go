package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/gozora/dedupfs"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

// extractCmd represents the extract command
var extractCmd = &cobra.Command{
	Use:   "extract [flags] pattern1 pattern2 ...",
	Short: "Extract file from archive",
	Long: `Extract file or whole directory from archive. 
Patterns are allowed to be used. The syntax of patterns is the same as in 
path.Match (https://pkg.go.dev/path/filepath#Match). 
The pattern may describe hierarchical names such as usr/*/bin/ed.`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		printIf.NotSilent.Println("Loading data...")
		myFs, err := dedupfs.Open(archiveName)
		if err != nil {
			return err
		}
		defer myFs.Destroy()

		myFs.Extracting = true

		v, err := cmd.Flags().GetBool("verbose")
		if err != nil {
			return err
		}
		if v {
			printIf.Gactive |= printIf.OptVerbose
		}

		dir, err := cmd.Flags().GetString("dst-dir")
		if err != nil {
			return err
		}

		err = myFs.Extract(args, dir)
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(extractCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// extractCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// extractCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	extractCmd.Flags().StringP("dst-dir", "d", "./", "Destination directory.")

	extractCmd.Flags().BoolP("verbose", "v", false,
		"Show files extracted from archive.")
}
