package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/gozora/dedupfs"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create [flags] pattern1 pattern2 ...",
	Short: "Create new de-duplicated archive",
	Long: `Create new deduplicated archive. 
Patterns are allowed to be used. The syntax of patterns is the same as in 
path.Match (https://pkg.go.dev/path/filepath#Match). 
The pattern may describe hierarchical names such as usr/*/bin/ed.
`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		v, err := cmd.Flags().GetBool("verbose")
		if err != nil {
			return err
		}
		if v {
			printIf.Gactive |= printIf.OptVerbose
		}

		// Don't allow to overwrite existing archive.
		v, err = cmd.Flags().GetBool("force")
		if err != nil {
			return err
		}

		if v {
			printIf.NotSilent.Println("Forced overwrite of archive by user.")
		}

		_, err = os.Stat(archiveName)
		if err == nil && !v {
			// This message should not be always loud.
			return fmt.Errorf("Archive \"%v\" already exists, use -f to override.",
				archiveName)
		}

		return dedupfs.Create(archiveName, args)
	},
}

func init() {
	rootCmd.AddCommand(createCmd)

	cdir, err := os.Getwd()
	if err != nil {
		printIf.NotSilent.Printf("Error can't get work directory: %v\n", err)
		return
	}

	createCmd.Flags().StringVarP(&dedupfs.GPrefixDir, "prefix-dir", "p", cdir,
		"Prefix archive file path with string.")

	createCmd.Flags().BoolP("verbose", "v", false,
		"Show files added to archive.")

	createCmd.Flags().BoolP("force", "f", false,
		"Allow overwriting of existing archive.")
}
