/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/gozora/dedupfs"

	"gitlab.com/gozora/dedupfs/printif"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List content of archive",
	Long: `List content (full file name as it can be accessed by HTTP) of archive. When no 
flags are specified, dump to stdout.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		v, err := cmd.Flags().GetBool("verbose")
		if err != nil {
			return err
		}
		if v {
			printif.Gactive |= printif.OptVerbose
		}

		myFs, err := dedupfs.Open(archiveName)
		if err != nil {
			return err
		}
		defer myFs.Destroy()

		fileName, err := cmd.Flags().GetString("file")
		if err != nil {
			return err
		}

		if fileName == "" {
			printIf.NotSilent.Println(myFs)
		} else {
			printIf.NotSilent.Printf("Writing file list to %s.\n", fileName)

			f, err := os.OpenFile(fileName,
				os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()

			f.WriteString(
				fmt.Sprintln(
					strings.Join(myFs.GetAllFiles(dedupfs.NamesOnly), "\n")))
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(listCmd)

	listCmd.Flags().StringP("file", "f", "", "Write output to file.")
	listCmd.Flags().BoolP("verbose", "v", false, "Show files added to archive.")
}
