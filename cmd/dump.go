/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/gozora/dedupfs"
	"gitlab.com/gozora/dedupfs/printif"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

// dumpCmd represents the dump command
var dumpCmd = &cobra.Command{
	Use:   "dump",
	Short: "Dump master index in JSON format",
	Long:  `Dump master index in JSON format. When no flags are specified, dump to stdout.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		v, err := cmd.Flags().GetBool("verbose")
		if err != nil {
			return err
		}
		if v {
			printif.Gactive |= printif.OptVerbose
		}

		myFs, err := dedupfs.Open(archiveName)
		if err != nil {
			return err
		}
		defer myFs.Destroy()

		fileName, err := cmd.Flags().GetString("file")
		if err != nil {
			return err
		}

		if fileName == "" {
			printIf.NotSilent.Println(myFs.Dump())
		} else {
			printIf.NotSilent.Printf("Dumping master index to %s"+
				" in JSON format...\n", fileName)

			f, err := os.OpenFile(fileName,
				os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()

			f.WriteString(fmt.Sprintln(myFs.Dump()))
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(dumpCmd)

	dumpCmd.Flags().StringP("file", "f", "",
		"Write output to file in JSON format")
	dumpCmd.Flags().BoolP("verbose", "v", false, "Show files added to archive.")
}
