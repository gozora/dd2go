package cmd

import (
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/gozora/dedupfs"
	"gitlab.com/gozora/dedupfs/printif"
)

var (
	version     string = "undefined" // set by ldflags when building
	archiveName string
	optSilent   bool
	optDebug    bool
)

var myName string = filepath.Base(os.Args[0])

// TODO: Support for serving multiple archives ?

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   myName,
	Short: "Create and serve de-duplicated archive over HTTP",
	Long:  `Create and serve de-duplicated archive over HTTP.`,
	Example: `Create archive:
	$ ` + myName + ` create --verbose  /bin/b* /tmp/p*
Serve created archive using HTTP:
	$ ` + myName + ` serve
List archive content:
	$ ` + myName + ` list
Extract content from archive:
	$ ` + myName + ` extract -v /bin/b*`,
	SilenceUsage: true,
	Version:      version,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if optSilent {
			printif.Gactive |= printif.OptSilent
		}

		if optDebug {
			printif.Gactive |= printif.OptDebug
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}

}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.dedup_main.yaml)")

	rootCmd.PersistentFlags().Int64VarP(&dedupfs.GBlockSz, "block-size", "b",
		dedupfs.GBlockSz, "Block size for file reading.")

	rootCmd.PersistentFlags().StringVarP(&archiveName, "archive", "a",
		"archive.db", "Name of archive to create")

	rootCmd.PersistentFlags().BoolVarP(&optDebug, "debug", "D",
		false, "Turn on debug messages")

	rootCmd.PersistentFlags().BoolVarP(&optSilent, "silent", "s",
		false, "Do not output anything")
}
