package myhttp

import (
	"net/http"
	"path"
	"strings"

	printIf "gitlab.com/gozora/dedupfs/printif"
)

const (
	ip = iota
	port
)

type wrapHandler struct {
	wrappedHandler http.Handler
}

func WrapHandler(handler http.Handler) http.Handler {
	return &wrapHandler{wrappedHandler: handler}
}

func (f *wrapHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	remoteAddr := strings.Split(r.RemoteAddr, ":")
	printIf.NotSilent.Printf("%v:%v\n", remoteAddr[ip], path.Clean(r.RequestURI))

	f.wrappedHandler.ServeHTTP(w, r)
}
